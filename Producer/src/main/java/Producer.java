
import java.util.Properties;

public class Producer {
    private final static String TOPIC_NAME = "demo-topic";
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());

        Logger logger = LoggerFactory.getLogger(Producer.class);

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        try {
            while (true) {
                String message = "Message " + System.currentTimeMillis();
                ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, message);

                producer.send(record, (metadata, exception) -> {
                    if (exception == null) {
                        logger.info("received new metadata, topic: " + metadata.topic()
                                + " partition: " + metadata.partition()
                                + " offsets: " + metadata.offset()
                                + " timestamp: " + metadata.timestamp());
                    } else {
                        logger.error("error producing: ", exception);
                    }
                });
                Thread.sleep(10);
            }
        } catch (Exception e) {
            System.out.println("Error while sending message: " + e);
        } finally {
            producer.close();
        }
    }
}
